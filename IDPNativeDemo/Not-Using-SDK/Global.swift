//
//  Global.swift
//  IDP2
//  Global的一些帮助方法
//  与IDPNativeAppSDK并无任何关系
//  Created by 九州云腾 on 16/9/2.
//  Copyright © 2016年 Michael Shang. All rights reserved.
//
let DecryptKeyPair  = [
    "DSjPLquC": "bHn4PIFFVQqkRW11",
    "WjPoW7MG": "KMUAHdOVqJSADJiH",
    "ofaVzCXQ": "wUbs63oHBwnKI5T9",
    "Xrb42Wgj": "gWCngTNtor8TaSNs",
    "O2XG7G8q": "YA0nVHg6uDxU38TA",
    "QaFnVCoE": "vDWFdzN3LP6ixbG1",
]
import UIKit
class Global: NSObject {
    
    static var huds: [MBProgressHUD] = [MBProgressHUD]();

    
    private static var __once: () = {
            Inner.instance = Global()
        }()
    
    class func rgba(_ r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor{
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a);
    }
    
    class func sharedInstance() -> Global {
        _ = Global.__once
        return Inner.instance!
    }
    
    struct Inner {
        static var instance: Global?
        static var token: Int = 0
    }
    
    class func instantiateViewControllerFromStoryboardID(_ storyboardID: String) -> UIViewController {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        return mainStoryboard.instantiateViewController(withIdentifier: storyboardID)
    }
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base!
        
    }
    
    class func showAlertViewWithController(_ controller : UIViewController,title : String, and: String, callback: ((UIAlertAction)->Void)?) {
        let alertController = UIAlertController.init(title: title, message: and, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "确定", style: .default) { (action: UIAlertAction) in
            if callback != nil {
                callback!(action);
            }
        }
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    
    }
    
    
    
    // 显示hud
    class func showProgressHUD(content: String) {
        DispatchQueue.main.async {
            if let topViewController = UIApplication.topViewController() {
                let hud: MBProgressHUD = MBProgressHUD.showAdded(to: topViewController.view, animated: true);
                hud.label.text = content;
                hud.label.font = UIFont.systemFont(ofSize: 15.0)
                
                huds.append(hud)
            }
        }
        
        
    }
    
    // 隐藏所有MBProgreeHUD
    class func hideProgressHUD() {
        DispatchQueue.main.async {
            for hud in Global.huds {
                hud.hide(animated: true);
            }
        }
    }
    
    
    

}




extension UIApplication {
    
    // return the top most view controller that is currently visible
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
    
}
