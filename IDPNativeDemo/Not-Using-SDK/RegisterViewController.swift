//
//  RegisterViewController.swift
//  IDPNativeApp
//  本VC只是一个简单的注册账号的机制，使用了LeanCloud提供的SDK存储用户信息
//  与IDPNativeAppSDK并无任何关系
//  Created by 九州云腾 on 16/11/11.
//  Copyright © 2016年 九州云腾. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var passwordConfirmTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func register(_ sender: Any) {
        
//        self.usernameTextField.text = "michael";
//        self.passwordTextfield.text = "12345678"
//        self.passwordConfirmTextfield.text = "12345678"
        
        if self.usernameTextField.text?.characters.count == 0
            || self.passwordTextfield.text?.characters.count == 0
            || self.passwordConfirmTextfield.text?.characters.count == 0 {
            Global.showAlertViewWithController(self, title: "失败", and: "用户名和密码信息不能为空", callback: nil);
            return;
        }
        
        if self.passwordTextfield.text != self.passwordConfirmTextfield.text {
            Global.showAlertViewWithController(self, title: "失败", and: "两次密码输入不一致", callback: nil);
            return;
        }
        
        // 注册用户
        let user : AVUser = AVUser.init()
        user.username = self.usernameTextField.text
        user.password = self.passwordTextfield.text
        user.signUpInBackground({ ( succeeded, error) in
            if succeeded{
                self.dismiss(animated: true, completion: {
                    ToastView.showToast(inParentView: UIApplication.topViewController()!.view, withText: "注册成功！", withDuaration: 4.0)
                });
            } else {

                print("注册失败\(error!.localizedDescription)")
                
                // 用户名已经被注册
                if error?._code == 202 {
                    Global.showAlertViewWithController(self, title: "注册失败", and: "用户名已经被注册", callback: nil)
                } else {
                    Global.showAlertViewWithController(self, title: "注册失败", and: "\(error!.localizedDescription)", callback: nil)
                }
            }
        })
        

    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
