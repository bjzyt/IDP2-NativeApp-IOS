//
//  UserInfoViewController.swift
//  IDPNativeApp
//  本VC只是一个简单的显示登录账号信息的界面，表示用户已经处在登录状态
//  与IDPNativeAppSDK并无任何关系
//  Created by 九州云腾 on 16/11/11.
//  Copyright © 2016年 九州云腾. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 显示用户已登录的信息
        self.messageLabel.text = " - 用户\(AVUser.current()!.username!) - \n已经使用九州云腾IDP产品提供的单点登录SDK成功登录！ "
    
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
}
