//
//  AppDelegate.swift
//  IDPNativeApp
//
//  Created by 九州云腾 on 16/11/8.
//  Copyright © 2016年 九州云腾. All rights reserved.
//

import UIKit


/**************************************************************************************/
/***************************************  import  *************************************/
/**************************************************************************************/


import IDPNativeAppSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // 初始化LeanCloud数据库链接
        AVOSCloud.setApplicationId("4ttO8fCifHlaOUEGO1J81I2x-gzGzoHsz", clientKey: "f3ya12Slu4fuxyoS2m0QiVO9")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}

   

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication]as! String
        if url.absoluteString.contains("downloadUrl") == true {

            return false
        }
        if url.absoluteString.contains("nativeToken") == false{

            return false
        }else{
        /**************************************************************************************/
        /********************************  接收处理跳转传过来的参数  ******************************/
        /**************************************************************************************/
        // IDPNativeAppSDK的方法，负责处理从IDP跳转过来的机制
        return IDPNativeAppSDK.handleOpenApp(url: url, sourceApplication: sourceApplication);
        }
    }

    }

