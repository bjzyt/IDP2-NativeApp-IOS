//
//  ViewController.swift
//  IDPNativeApp
//
//  Created by 九州云腾 on 16/11/8.
//  Copyright © 2016年 九州云腾. All rights reserved.
//

import UIKit

/**************************************************************************************/
/***************************************  import  *************************************/
/**************************************************************************************/
//import IDPIDPNativeAppSDK

import IDPNativeAppSDK

class LoginViewController: UIViewController {
    
    // 用户名textfield
    @IBOutlet weak var username: UITextField!
    
    // 密码textfield
    @IBOutlet weak var password: UITextField!

    @IBOutlet weak var version: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /**************************************************************************************/
        /************************************  打开app行为  *************************************/
        /**************************************************************************************/
        // 如果想要实现在除了单点登录之外的某一时刻获取一次账号信息，则不需要这行
        // 如果想要实现从IDP单点登录，那么必须要下面这行
        // SDK中的代码已经检查过是否是IDP中跳转打开，只有从IDP中单点登录打开的情况下getUserInformation()才会返回用户信息，其余时候什么都不做，不会影响到其他代码逻辑
        let versionDic =  Bundle.main.infoDictionary! as [String : Any]
        
        let version :String = versionDic["CFBundleShortVersionString"] as! String
        
        self.version.text = "Version \(version)"
        
        NotificationCenter.default.addObserver(self, selector:#selector(LoginViewController.automaticLogin), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    // 自动单点登录
    @objc func automaticLogin() {
        
        /**************************************************************************************/
        /************************************  获取账号信息  *************************************/
        /**************************************************************************************/
        // 本SDK最核心的一个方法，获取跳转过来的账号信息
        // 注意，跳转过来以后，这个方法只会有效一次
        let sdk = IDPNativeAppSDK.init()

        sdk.getUserInformation(successBlock: { (response: Dictionary<String, Any>) in
            
            if let username = response["username"] as? String, let password = response["password"] as? String {
                self.login(username: username, password: password);
            }
            
        }, failureBlock:{ (response: Dictionary<String, Any>) in
            Global.showAlertViewWithController(self, title: "错误", and: "\(response["detail"]!)", callback: nil);
        });
        

    }
    

    
    // 登录按钮点击事件
    @IBAction func loginClick(_ sender: AnyObject) {
        self.login(username: self.username.text!, password: self.password.text!);
    }
        
    
    // 登录
    // 可以看出，无论是自带的登录还是跳转单点登录，都可以使用同样的方法，改造代价非常小
    // 本demo中，数据库直接使用的是LeanCloud，这里的AVUser就是它的用户Model
    func login(username: String, password: String) {
        
        if username.characters.count == 0
            || password.characters.count == 0 {
            Global.showAlertViewWithController(self, title: "失败", and: "用户名和密码信息不能为空", callback: nil);
            return;
        }
        
        Global.showProgressHUD(content: "登录中");
        
        // 登录ß
        AVUser.logInWithUsername(inBackground: username, password: password) { (user, error) in
            Global.hideProgressHUD();
            
            
            if (error != nil) {
                print("登录失败\(error!.localizedDescription)")
                
                // 用户名已经被注册
                if error?._code == 210 {
                    Global.showAlertViewWithController(self, title: "登录失败", and: "用户名或密码错误", callback: nil)
                } else if error?._code == 211 {
                    Global.showAlertViewWithController(self, title: "登录失败", and: "用户名还未被注册", callback: nil)
                } else {
                    Global.showAlertViewWithController(self, title: "登录失败", and: "\(error!.localizedDescription)", callback: nil)
                }
            
            } else{
                // 登录成功
                
                Global.hideProgressHUD();

                let userVC = Global.instantiateViewControllerFromStoryboardID("UserInfo") as! UserInfoViewController;
                let navigationController: UINavigationController = UINavigationController(rootViewController: userVC);
                navigationController.modalTransitionStyle = .crossDissolve

                self.present(navigationController, animated: true, completion: nil);

            }
        }
    }
    
    
    // 打开注册界面
    @IBAction func registerClick(_ sender: AnyObject) {
        
        let registerVC = Global.instantiateViewControllerFromStoryboardID("Register") as! RegisterViewController;
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: registerVC);
        self.present(navigationController, animated: true, completion: nil);

    }


    @IBAction func ssoLoginWithIDP2(_ sender: UITapGestureRecognizer) {

        IDPNativeAppSDK.open(scheme: "jiuzhou", appType: IDPNativeAppType.IDP_BasicNativeApp.rawValue,appURLScheme:"JZYTiOSNative") { (success) in
            print(success)
        }
    }
}

