//
//  IDPNativeAppSDK.h
//  IDPNativeAppSDK
//
//  Created by Michael Shang on 11/21/16.
//  Copyright © 2016 com.idsmanager. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTMBase64U2F.h"
#import "NSData+AES.h"

//! Project version number for IDPNativeAppSDK.
FOUNDATION_EXPORT double IDPNativeAppSDKVersionNumber;

//! Project version string for IDPNativeAppSDK.
FOUNDATION_EXPORT const unsigned char IDPNativeAppSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IDPNativeAppSDK/PublicHeader.h>


